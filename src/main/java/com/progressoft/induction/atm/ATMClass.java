package com.progressoft.induction.atm;

import com.progressoft.induction.atm.exceptions.AccountNotFoundException;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.progressoft.induction.atm.Banknote.*;

public class ATMClass implements ATM {
    BankingSystemClass bankingSystem = new BankingSystemClass();
    Map<String, AccountClass> database = bankingSystem.database;
    BigDecimal totalAmountInAtm;
    List<Banknote> returnedNotes = new ArrayList<>();
    Map<BigDecimal, BigDecimal> bankNoteCount = new HashMap<>();
    int counter;
    double doubleAmount;

    public ATMClass() {
        this.initializeMap();
        this.totalAmountInAtm = totalAmountInAtm();
    }


    @Override
    public List<Banknote> withdraw(String accountNumber, BigDecimal amount) {
        doubleAmount = amount.doubleValue();
        throwIfAccountNotFound(accountNumber);
        throwIfInsufficientFunds(accountNumber, amount);
        throwIfNotEnoughMoneyInATM(amount);
        returnedBankNotes();
        totalAmountInAtm = totalAmountInAtm.subtract(amount);
        bankingSystem.debitAccount(accountNumber,amount);

        return returnedNotes;
    }

    private void initializeMap() {
        this.bankNoteCount.put(FIFTY_JOD.getValue(),new BigDecimal("10"));
        this.bankNoteCount.put(TWENTY_JOD.getValue(),new BigDecimal("20"));
        this.bankNoteCount.put(TEN_JOD.getValue(),new BigDecimal("100"));
        this.bankNoteCount.put(FIVE_JOD.getValue(),new BigDecimal("100"));
    }

    private BigDecimal totalAmountInAtm (){
        BigDecimal totalValueOfNotes = new BigDecimal("0.0");
        BigDecimal sumOfFiftyJod= bankNoteCount.get(FIFTY_JOD.getValue()).multiply(FIFTY_JOD.getValue());
        BigDecimal sumOfTwentyJod= bankNoteCount.get(TWENTY_JOD.getValue()).multiply(TWENTY_JOD.getValue());
        BigDecimal sumOfTenJod= bankNoteCount.get(TEN_JOD.getValue()).multiply(TEN_JOD.getValue());
        BigDecimal sumOfFiveJod= bankNoteCount.get(FIVE_JOD.getValue()).multiply(FIVE_JOD.getValue());
        totalValueOfNotes = totalValueOfNotes.add(sumOfFiveJod.add(sumOfTenJod.add(sumOfTwentyJod.add(sumOfFiftyJod))));

        return totalValueOfNotes;
    }

    private void returnedBankNotes() {
        calculateBankNotesCount(FIFTY_JOD);
        calculateBankNotesCount(TWENTY_JOD);
        calculateBankNotesCount(TEN_JOD);
        calculateBankNotesCount(FIVE_JOD);
    }

    private void calculateBankNotesCount(Banknote currency){
        counter = bankNoteCount.get(currency.getValue()).intValue();
        while (doubleAmount >= 5 && counter > 0) {
            returnedNotes.add(currency);
            doubleAmount -= currency.getValue().doubleValue();
            bankNoteCount.replace(FIVE_JOD.getValue(),bankNoteCount.get(FIVE_JOD.getValue()),new BigDecimal(counter));
        }
    }

    private void throwIfNotEnoughMoneyInATM(BigDecimal amount) {
        if (totalAmountInAtm.compareTo(amount) < 0) {
            throw new NotEnoughMoneyInATMException();
        }
    }

    private void throwIfInsufficientFunds(String accountNumber, BigDecimal amount) {
        if (bankingSystem.getAccountBalance(accountNumber).compareTo(amount) < 0) {
            throw new InsufficientFundsException();
        }
        if (database.get(accountNumber).getBalance().compareTo(amount) < 0) {
            throw new InsufficientFundsException();
        }
    }

    private void throwIfAccountNotFound(String accountNumber) {
        if (!database.containsKey(accountNumber)) {
            throw new AccountNotFoundException();
        }
    }
}
