package com.progressoft.induction.atm;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class BankingSystemClass implements BankingSystem{
    public BankingSystemClass(){
        initializeMap();
    }
    public Map<String, AccountClass> database = new HashMap<>();
    private void initializeMap(){
        this.database.put("123456789",new AccountClass("123456789", new BigDecimal("1000.0")));
        this.database.put("111111111",new AccountClass("111111111", new BigDecimal("1000.0")));
        this.database.put("222222222",new AccountClass("222222222", new BigDecimal("1000.0")));
        this.database.put("333333333",new AccountClass("333333333", new BigDecimal("1000.0")));
        this.database.put("444444444",new AccountClass("444444444", new BigDecimal("1000.0")));
    }
    @Override
    public BigDecimal getAccountBalance(String accountNumber) {
        return database.get(accountNumber).getBalance();
    }

    @Override
    public void debitAccount(String accountNumber, BigDecimal amount) {
        BigDecimal newAmount =this.database.get(accountNumber).getBalance().subtract(amount);
        this.database.get(accountNumber).setBalance(newAmount);
    }
}
