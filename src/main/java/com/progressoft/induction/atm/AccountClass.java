package com.progressoft.induction.atm;

import java.math.BigDecimal;

public class AccountClass {
    private String accountNumber;
    private BigDecimal balance;
    public AccountClass(String accountNum, BigDecimal balance){
        this.accountNumber= accountNum;
        this.balance = balance;
    }
    public String getAccountNumber() {
        return accountNumber;
    }
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }
    public BigDecimal getBalance() {
        return balance;
    }
    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
